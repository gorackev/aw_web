-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2017. Sze 16. 12:09
-- Kiszolgáló verziója: 5.7.14
-- PHP verzió: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `animeworld`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `news`
--

CREATE TABLE `news` (
  `id` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text COLLATE utf8_hungarian_ci NOT NULL,
  `cover` text COLLATE utf8_hungarian_ci NOT NULL,
  `images` text COLLATE utf8_hungarian_ci,
  `userId` varchar(32) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `news`
--

INSERT INTO `news` (`id`, `title`, `url`, `date`, `content`, `cover`, `images`, `userId`) VALUES
('15ff389db0a19a1eaef0e6191989a9c1', 'Teszt', 'teszt', '2017-09-02 07:27:10', 'Ez itt a teszt bejegyzés!', '/img/default.png', NULL, '1'),
('34c7f19b274fe89185cb8b2e4bfd4c8d', 'Teszt poszt - 3', 'teszt_poszt_3', '2017-09-02 07:39:20', 'Ez itt a harmadik teszt bejegyzés!', '/img/default.png', NULL, '1'),
('9d4d55fb58d0309dc4faeef2215bf3d6', 'Teszt poszt - 2', 'teszt_poszt_2', '2017-09-02 07:28:20', 'Ez itt a második teszt bejegyzés!', '/img/default.png', NULL, '1'),
('dd92a72ae21b57674ed583a6f15a7ca3', 'Teszt bejegyzes 4', 'teszt_bejegyzes_4', '2017-09-02 09:58:18', 'Ez itt a negyedik teszt bejegyzés!', '/img/default.png', NULL, '1');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `projects`
--

CREATE TABLE `projects` (
  `id` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  `titleJap` text COLLATE utf8_hungarian_ci NOT NULL,
  `titleEng` text COLLATE utf8_hungarian_ci NOT NULL,
  `url` text COLLATE utf8_hungarian_ci NOT NULL,
  `typeId` int(1) NOT NULL,
  `firstEpDate` timestamp NULL DEFAULT NULL,
  `lastEpDate` timestamp NULL DEFAULT NULL,
  `started` timestamp NULL DEFAULT NULL,
  `ended` timestamp NULL DEFAULT NULL,
  `episodeNum` int(3) NOT NULL,
  `episodeLength` int(3) NOT NULL,
  `description` text COLLATE utf8_hungarian_ci,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `projects`
--

INSERT INTO `projects` (`id`, `titleJap`, `titleEng`, `url`, `typeId`, `firstEpDate`, `lastEpDate`, `started`, `ended`, `episodeNum`, `episodeLength`, `description`, `date`) VALUES
('6857e61a8075ca97ba64197271319732', 'Blue Exorcist', 'Ao no Exorcist', 'blue_exorcist', 1, NULL, NULL, NULL, NULL, 25, 25, NULL, '2017-08-29 08:15:49'),
('69cb29a74b1e931c135ce17cdd355cb4', 'Shingeki no Kyochin', 'Attack on Titan', 'shingeki no kyochin', 1, NULL, NULL, NULL, NULL, 12, 24, NULL, '2017-08-29 06:34:56'),
('6e21302c4006637d6a02cec4ee31e4fe', 'Gamers!', 'Gamers!', 'gamers!', 1, NULL, NULL, NULL, NULL, 12, 25, NULL, '2017-08-29 08:15:02');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  `username` varchar(12) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `fullname` varchar(20) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `email` varchar(25) COLLATE utf8_hungarian_ci NOT NULL,
  `regdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `birthdate` date NOT NULL,
  `cover` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `permission` int(1) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fullname`, `email`, `regdate`, `birthdate`, `cover`, `permission`) VALUES
('988fe6d80206e31ddca08108d0bbd149', 'Gorack', '$2y$08$MGVKWDZ1dnhLSWlXeXZFYOhLF8wZ35Eib.RqXroLcLgr6jho.FdYW', NULL, 'gorack16@gmail.com', '2017-09-13 06:04:41', '1994-12-30', NULL, 2),
('f8957be1c4907a0ba5a35624d5838acc', 'Gorack1', '$2y$08$eEp3RW1Gb2NwaGNCREdjQut5Y0Kmbp3xCvpa7DnWXaXiaB6V1QAai', NULL, 'gorack32@gmail.com', '2017-09-13 07:08:47', '1994-12-30', NULL, 2);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
