<?php

use Phalcon\Mvc\Controller;

abstract class ControllerBase extends Controller
{
    public $user;

    public function onConstruct(){
        if($this->router->getActionName()){
            if(!method_exists($this, $this->router->getActionName()."Action")){
                $this->response->redirect('/news');
                $this->response->send();
            }
        }

        //SET USER VARIABLE
        $this->setUser();

        $this->checkAccessLevel();


        //DEFINE PAGE TITLE
        $this->setTitle();

        //ADD CSS FILES
        $this->assets->addCss('/css/animate.css');
        $this->assets->addCss('/css/font-awesome.css');
        $this->assets->addCss('/css/main.css');

        //ADD JS FILES
        $this->assets->addJs('/js/jquery-slim.min.js');
        $this->assets->addJs('/js/popper.min.js');
        $this->assets->addJs('/js/bootstrap.min.js');
        $this->assets->addJs('/js/ckeditor/ckeditor.js');

        //ENABLE SIDEBAR
        $this->enableSidebar();

        //SET ISADMIN VAR NULL
        $this->view->isAdmin = ($this->isAdmin()) ? true : false;

        //GET LATEST 5 POST FOR SIDEBAR
        $this->view->latestNews = Posts::find([
            'order' => 'date DESC',
            'limit' => 5
        ]);

        $this->view->projectFilter = false;
        $this->view->latestNewsCheck = true;

    }

    /**
     * CHECK USERS ACCESS LEVEL FOR CONTROLLERS
     *
     * LEVEL 0: JUST ADMIN
     * LEVEL 1: JUST LOGGED USER
     * LEVEL 2: JUST GUEST USER
     */
    private function checkAccessLevel(){
        if(isset($this->permission)){
            switch ($this->permission){
                case 0:
                    if(!$this->isAdmin()){
                        $this->response->redirect('/news');
                        $this->response->send();
                    }
                    break;
                case 1:
                    if(!$this->checkUser()){
                        $this->response->redirect('/news');
                        $this->response->send();
                    }
                    break;
                case 2:
                    if($this->checkUser()){
                        $this->response->redirect('/news');
                        $this->response->send();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param null $title
     *
     * DEFINE PAGE TITLE FROM CONFIG OR SET MANUAL
     */
    public function setTitle($title = null){
        $class = substr(get_called_class(), 0, strpos(get_called_class(), 'Controller'));
        $class = strtolower($class);

        if(!$title) $title = $this->config->titles->$class;

        $this->view->title = $title.$this->config->titles->main;
    }

    public function checkUser(){
        if($this->session->has('userId') || $this->cookies->has('remember')) return true;
            else return false;
    }

    public function getUser(){
        $id = null;
        if($this->session->has('userId')) $id = $this->session->get('userId');
            elseif($this->cookies->has('remember')) $id = $this->cookies->get('remember');

        return Users::findFirst([
            'id = "'.$id.'"'
        ]);
    }

    private function setUser(){
        if($this->checkUser()) {
            $this->user = $this->getUser();
            $this->view->user = $this->getUser();
        } else $this->view->user = NULL;
    }

    public function enableSidebar(){
        $this->view->sidebar = true;
    }

    public function disableSidebar(){
        $this->view->sidebar = false;
    }

    public function isAdmin(){
        if($this->checkUser()){
            if($this->user->permission == 0) return true;
                else return false;
        } else return false;
    }

    public function setDefaultInputMessages(){
        $this->view->messages = new stdClass();
        foreach ($this->view->form->getElements() as $key => $value){
            $this->view->messages->{$key} = "";
        }
    }

    public function setInputMessages($field, $message){
        $this->view->messages->{$field} = "<i class='fa fa-exclamation-triangle fa-fw'></i>".$message;
    }

    public function getFormMessages(){
        return $this->view->form->getMessages();
    }
}
