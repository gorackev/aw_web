<?php

class NewsController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->news = Posts::find(
            [
                'public = 1',
                'order' => 'date DESC'
            ]
        );
    }

    public function categoryAction(){
        $category = $this->dispatcher->getParam('category');
        $ids = [];

        $categoryId = Postcategories::findFirst([
            'url = "'.$category.'"'
        ])->id;

        if($categoryId){
            $postIds = PostCategory::find([
                'categoryId = "'.$categoryId.'"'
            ]);

            foreach ($postIds as $postId){
                array_push($ids, $postId->postId);
            }

            $posts = Posts::find([
                'id IN ({ids:array})',
                'bind' => [
                    'ids' => $ids
                ],
                'order' => 'date DESC'
            ]);

            $this->view->news = $posts;

            $this->view->pick('news/index');
        } else {
            $this->response->redirect('/news');
            $this->response->send();
        }
    }

    public function showAction(){
        $url = $this->dispatcher->getParam('url');
        $where = "url = '{$url}'";

        parent::disableSidebar();

        if(count(Posts::find($where))){
            $new = Posts::findFirst($where);
            $this->setTitle($new->title);
            $this->view->post = $new;
        } else {
            $this->response->redirect('/news');
            $this->response->send();
        }
    }

    public function testAction(){
        $this->view->disable();
    }
}

