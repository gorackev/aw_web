<?php

class RegistrationController extends ControllerBase
{
    public $permission = 2;

    public function indexAction()
    {
        $this->view->form = new RegistrationForm();
        $this->setDefaultInputMessages();
        $request = $this->request;

        if($request->isPost()){
            $error = true;
            if(!$this->view->form->isValid($request->getPost())){
                foreach (parent::getFormMessages() as $message){
                    $this->view->form->setMessages([$message->getField() => $message->getMessage()]);
                }
                $error = false;
            }

            if(!Functions::checkHasFiles($this->request->getUploadedFiles())){
                $error = false;
                $this->view->form->setMessages(['avatar' => 'Az avatar kép kiválasztása kötelező!']);
            }

            if($error) $this->doRegistration();
        }
    }

    private function doRegistration(){
        $user = new Users();

        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        $user->id = Functions::getIdFormat($username);

        foreach ($this->request->getUploadedFiles() as $file){
            $avatarName = $user->id.$this->config->images->mimeTypes->{$file->getRealType()}->extension;
            $fullPath = $this->config->images->paths->userAvatars.$avatarName;
            $move = $file->moveTo($fullPath);
        }

        if($move){
            $user->password = $this->security->hash($password);
            $user->cover = '/'.$fullPath;

            if($user->save($this->request->getPost(), ['username', 'email', 'birthdate'])) $this->flashSession->success('Sikeresen regisztráltál. Most már beléphetsz!');
                else {
                    $this->flashSession->error('A regisztráció során hiba lépett fel. Próbáld meg újra!');
                    $this->response->redirect('/news');
                    $this->response->send();
                }
        } else $this->view->form->setMessages(['avatar' => 'A feltöltés során hiba lépett fel. Próbáld meg újra!']);
    }

}

