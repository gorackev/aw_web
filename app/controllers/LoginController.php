<?php

class LoginController extends ControllerBase
{
    public $permission = 2;

    public function indexAction()
    {
        $form = new LoginForm();
        $this->view->form = $form;
        $this->setDefaultInputMessages();

        if($this->request->isPost()) $this->doLogin();
    }

    private function doLogin(){
        if(!$this->view->form->isValid($this->request->getPost())){
            foreach (parent::getFormMessages() as $message){
                $this->view->form->setMessages([$message->getField() => $message->getMessage()]);
            }
        } else {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $user = Users::findFirst("username = '".$username."'");
            $error = false;

            if(!$user){
                $this->view->messages->username = "Nem létezik ilyen felhasználónévvel fiók!";
                $error = true;
            }

            if(!$this->security->checkHash($password, $user->password)){
                $this->view->messages->password = "A megadott jelszó helytelen!";
                $error = true;
            }

            if(!$error){
                if($this->request->get('remember') == "on") $this->cookies->set('remember', $user->id, time() + 15 * 86400);
                    else $this->session->set('userId', $user->id);

                $this->response->redirect('/news');
                $this->response->send();
            }
        }
    }

}

