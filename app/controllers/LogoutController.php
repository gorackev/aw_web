<?php

class LogoutController extends ControllerBase
{
    public $permission = 1;

    public function indexAction()
    {
        $this->view->disable();

        $this->session->destroy();
        $remember = $this->cookies->get('remember');
        $remember->delete();

        $this->response->redirect('/news');
        $this->response->send();
    }

}

