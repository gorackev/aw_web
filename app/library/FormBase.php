<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 10. 02.
 * Time: 8:59
 */

use Phalcon\Forms\Form;


abstract class FormBase extends Form
{
    public static $messages = [];

    public function renderDecorated($name){
        $element = $this->get($name);
        $this->setDefaultMessage($element);

        echo '
        <div class="form-group">
            <label for="'.$element->getName().'"><b>'.$element->getLabel().'</b></label>
            '.$element.'
            <small class="form-text text-danger">'.self::$messages[$element->getName()].'</small>
            <hr>
        </div>
        ';
    }

    public function setLabels($array = []){
        foreach ($array as $key => $value) {
            $this->$key->setLabel($value);
        }
    }

    public function addElements($elements = []){
        foreach ($elements as $element){
            $this->add($this->$element);
        }
    }

    private function setDefaultMessage($element){
        if(!isset(self::$messages[$element->getName()])) self::$messages[$element->getName()] = "";
    }

    public static function setMessages($elements = []){
        foreach ($elements as $element => $message){
            self::$messages[$element] = $message;
        }
    }
}