<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 09. 12.
 * Time: 11:04
 */

use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Submit;

use Phalcon\Validation\Validator\Callback;

class RegistrationForm extends AuthForm
{
    public $avatar;

    public function initialize(){
        parent::initialize();

        $this->send = new Submit(
            'register',
            [
                'class' => 'btn btn-primary',
                'value' => 'Regisztrálok'
            ]
        );

        $this->avatar = new File(
            'avatar',
            [
                'class' => 'form-control'
            ]
        );

        $this->username->addValidator(
            new Callback(
                [
                    'message' => 'A megadott felhasználónévvel már létezik fiók!',
                    'callback' => function($data){
                        if(count(Users::find(['username = "'.$data['username'].'"']))) return false;

                        return true;
                    }
                ]
            )
        );

        $this->email->addValidator(
            new Callback(
                [
                    'message' => 'A megadott e-mail címmel már létezik fiók!',
                    'callback' => function($data){
                        if(count(Users::find(['email = "'.$data['email'].'"']))) return false;

                        return true;
                    }
                ]
            )
        );

        $this->setLabels([
            'avatar' => 'Avatar kép'
        ]);

        $this->addElements([
            'avatar', 'username', 'password', 'passwordConfirm', 'email', 'emailConfirm', 'birthDate', 'send'
        ]);
    }
}