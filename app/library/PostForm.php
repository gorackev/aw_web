<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 09. 17.
 * Time: 9:12
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Submit;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Confirmation;

class PostForm extends FormBase
{
    public $title;
    public $precursory;
    public $content;
    public $cover;
    public $category;
    public $send;

    public function initialize(){
        $this->title = new Text(
            'title',
            [
                'placeholder' => 'Hír címe',
                'class' => 'form-control'
            ]
        );

        $this->precursory = new Textarea(
            'precursory',
            [
                'placeholder' => 'Előzetes szöveg',
                'class' => 'form-control'
            ]
        );

        $this->content = new Textarea(
            'content',
            [
                'placeholder' => 'Szöveg',
                'class' => 'form-control',
                'id' => 'editor'
            ]
        );

        $this->category = new Select(
            'categoryId',
            Postcategories::find(),
            [
                'using' => [
                    'id', 'name'
                ],
                'class' => 'form-control'
            ]
        );

        $this->cover = new File(
            'cover',
            [
                'class' => 'form-control'
            ]
        );

        $this->send = new Submit(
            'send',
            [
                'value' => 'Hír beküldése',
                'class' => 'btn btn-primary'
            ]
        );

        //VALIDATIONS

        $this->title->addValidator(
            new PresenceOf(
                [
                    'message' => 'A címet kötelező megadni!'
                ]
            )
        );

        $this->precursory->addValidator(
            new PresenceOf(
                [
                    'message' => 'Előzetes szöveget kötelező megadni!'
                ]
            )
        );

        $this->setLabels([
            'title' => 'Cím',
            'precursory' => 'Előnézet',
            'content' => 'Tartalom',
            'category' => 'Kategória',
            'cover' => 'Borító'
        ]);

        //ADD INPUTS TO FORM

        $this->addElements([
            'title', 'precursory', 'content', 'category', 'cover', 'send'
        ]);
    }
}