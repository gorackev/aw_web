<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 09. 26.
 * Time: 7:16
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;

use Phalcon\Validation\Validator\PresenceOf;

class ProjectGenreForm extends Form
{
    public $name;
    public $send;

    public function initialize(){
        $this->name = new Text(
            'name',
            [
                'placeholder' => 'Műfaj neve',
                'class' => 'form-control',
                'autocomplete' => 'off'
            ]
        );

        $this->send = new Submit(
            'send',
            [
                'value' => 'Küldés',
                'class' => 'btn btn-primary'
            ]
        );

        $this->name->addValidator(
            new PresenceOf(
                [
                    'message' => 'A műfaj nevét kötelező megadni!'
                ]
            )
        );

        $this->add($this->name);
        $this->add($this->send);
    }

}