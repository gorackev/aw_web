<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 09. 11.
 * Time: 6:15
 */

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Date;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Message;

class AuthForm extends FormBase
{
    public $username;
    public $email;
    public $emailConfirm;
    public $password;
    public $passwordConfirm;
    public $birthDate;

    public function initialize(){
        $this->username = new Text(
            'username',
            [
                'class' => 'form-control',
                'placeholder' => 'Felhasználónév',
                'autocomplete' => 'off',
                'autofocus' => 'on'
            ]
        );
        $this->email = new Email(
            'email',
            [
                'class' => 'form-control',
                'placeholder' => 'E-mail cím',
                'autocomplete' => 'off'
            ]
        );
        $this->emailConfirm = new Email(
            'emailConfirm',
            [
                'class' => 'form-control',
                'placeholder' => 'E-mail cím megerősítése',
                'autocomplete' => 'off'
            ]
        );
        $this->password = new Password(
            'password',
            [
                'class' => 'form-control',
                'placeholder' => 'Jelszó'
            ]
        );
        $this->passwordConfirm = new Password(
            'passwordConfirm',
            [
                'class' => 'form-control',
                'placeholder' => 'Jelszó megerősítése'
            ]
        );
        $this->birthDate = new Date(
            'birthdate',
            [
                'class' => 'form-control',
                'placeholder' => 'Születési dátum'
            ]
        );

        $this->username->addValidator(
            new PresenceOf(
                [
                    'message' => 'A felhasználónév megadása kötelező!'
                ]
            )
        );

        $this->username->setFilters([
            'string',
            'trim'
        ]);

        $this->email->addValidator(
            new PresenceOf(
                [
                    'message' => 'Az e-mail cím megadása kötelező!'
                ]
            )
        );

        $this->email->addValidator(
            new EmailValidator(
                [
                    'message' => 'Az e-mail cím formátuma nem megfelelő!'
                ]
            )
        );

        $this->emailConfirm->addValidator(
            new Confirmation([
                'message' => 'A megadott e-mail címek nem egyeznek!',
                'with' => 'email'
            ])
        );

        $this->emailConfirm->addValidator(
            new PresenceOf(
                [
                    'message' => 'Az e-mail cím megerősítés megadása kötelező!'
                ]
            )
        );

        $this->password->addValidator(
            new PresenceOf(
                [
                    'message' => 'A jelszó megadása kötelező!'
                ]
            )
        );

        $this->password->setFilters([
            'string',
            'trim'
        ]);

        $this->passwordConfirm->addValidator(
            new PresenceOf(
                [
                    'message' => 'A jelszó megerősítés megadása kötelező!'
                ]
            )
        );

        $this->passwordConfirm->addValidator(
            new Confirmation(
                [
                    'message' => 'A megadott jelszavak nem egyeznek!',
                    'with' => 'password'
                ]
            )
        );

        $this->birthDate->addValidator(
            new PresenceOf(
                [
                    'message' => 'A születési dátum megadása kötelező!'
                ]
            )
        );

        $this->setLabels([
            'username'          => 'Felhasználónév',
            'password'          => 'Jelszó',
            'passwordConfirm'   => 'Jelszó megerősítése',
            'email'             => 'E-mail cím',
            'emailConfirm'      => 'E-mail cím megerősítése',
            'birthDate'         => 'Születési dátum'
        ]);
    }
}