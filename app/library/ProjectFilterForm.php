<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 09. 27.
 * Time: 11:53
 */


use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Select;

use Phalcon\Validation\Validator\Callback;

class ProjectFilterForm extends FormBase
{
    public $type;
    public $status;
    public $startYear;
    public $endYear;
    public $send;

    public function initialize(){
        $this->type = new Select(
            'type',
            ProjectTypes::find(),
            [
                'useEmpty' => true,
                'emptyText' => 'Összes',
                'using' => [
                    'id', 'name'
                ],
                'class' => 'form-control'
            ]
        );
        $this->status = new Select(
            'status',
            ProjectStates::find(),
            [
                'using' => ['id', 'name'],
                'useEmpty' => true,
                'emptyText' => 'Összes',
                'class' => 'form-control'
            ]
        );
        $this->startYear = new Numeric(
            'startyear',
            [
                'class' => 'form-control',
                'placeholder' => 'Kezdő év'
            ]
        );
        $this->endYear = new Numeric(
            'endyear',
            [
                'class' => 'form-control',
                'placeholder' => 'Záró év'
            ]
        );
        $this->send = new Submit(
            'send',
            [
                'class' => 'btn btn-primary',
                'value' => 'Szűrés'
            ]
        );

        $this->setLabels([
            'type'      => 'Típus',
            'status'    => 'Állapot',
            'startYear' => 'Kezdő év',
            'endYear'   => 'Záró év'
        ]);

        $this->startYear->addValidator(
            new Callback([
                'message' => 'Helytelen év formátum!',
                'callback' => function($data){
                    if(strlen($data['startYear']) != 4) return false;

                    return true;
                }
            ])
        );

        $this->endYear->addValidator(
            new Callback([
                'message' => 'Helytelen év formátum!',
                'callback' => function($data){
                    if(strlen($data['endYear']) != 4) return false;

                    return true;
                }
            ])
        );

        $this->addElements([
            'status', 'type', 'startYear', 'endYear', 'send'
        ]);
    }
}