<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 09. 26.
 * Time: 6:32
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Numeric;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Callback;

class ProjectForm extends FormBase
{
    public $titlejap;
    public $titleeng;
    public $firstepdate;
    public $lastepdate;
    public $startdate;
    public $finishdate;
    public $episodenum;
    public $episodelength;
    public $description;
    public $cover;
    public $type;
    public $status;
    public $send;

    public function initialize(){

        //DEFINES

        $this->titlejap = new Text(
            'titlejap',
            [
                'placeholder' => 'Japán cím',
                'class' => 'form-control',
                'autocomplete' => 'off',
                'autofocus' => 'on'
            ]
        );
        $this->titleeng = new Text(
            'titleeng',
            [
                'placeholder' => 'Angol cím',
                'class' => 'form-control',
                'autocomplete' => 'off'
            ]
        );
        $this->firstepdate = new Date(
            'firstepdate',
            [
                'class' => 'form-control'
            ]
        );
        $this->lastepdate = new Date(
            'lastepdate',
            [
                'class' => 'form-control'
            ]
        );
        $this->startdate = new Date(
            'startdate',
            [
                'class' => 'form-control'
            ]
        );
        $this->finishdate = new Date(
            'finishdate',
            [
                'class' => 'form-control'
            ]
        );
        $this->episodenum = new Numeric(
            'episodenum',
            [
                'placeholder' => 'Epizódok száma',
                'class' => 'form-control',
                'autocomplete' => 'off'
            ]
        );
        $this->episodelength = new Numeric(
            'episodelength',
            [
                'placeholder' => 'Epizódok hossza',
                'class' => 'form-control',
                'autocomplete' => 'off'
            ]
        );
        $this->description = new TextArea(
            'description',
            [
                'placeholder' => 'Leírás',
                'class' => 'form-control',
                'id' => 'editor'
            ]
        );
        $this->cover = new File(
            'cover',
            [
                'class' => 'form-control',
            ]
        );
        $this->type = new Select(
            'type',
            ProjectTypes::find(),
            [
                'using' => [
                    'id', 'name'
                ],
                'class' => 'form-control'
            ]
        );
        $this->status = new Select(
            'status',
            ProjectStates::find(),
            [
                'using' => [
                    'id', 'name'
                ],
                'class' => 'form-control'
            ]
        );
        $this->send = new Submit(
            'send',
            [
                'value' => 'Feltöltés',
                'class' => 'btn btn-primary'
            ]
        );

        //VALIDATIONS

        $this->titlejap->addValidator(
            new PresenceOf(
                [
                    'message' => 'A japán címet kötelező megadni!'
                ]
            )
        );

        $this->titlejap->addValidator(
            new Callback(
                [
                    'message' => 'A megadott japán címmel létezik projekt!',
                    'callback' => function($data){
                        $projects = Projects::find([
                            'titlejap = "'.$data['titlejap'].'"'
                        ]);

                        if(count($projects)) return false;

                        return true;
                    }
                ]
            )
        );

        $this->titleeng->addValidator(
            new Callback(
                [
                    'message' => 'A megadott angol címmel létezik projekt!',
                    'callback' => function($data){
                        if($data['titleeng']){
                            $projects = Projects::find([
                                'titleeng = "'.$data['titleeng'].'"'
                            ]);

                            if(count($projects)) return false;
                        }

                        return true;
                    }
                ]
            )
        );

        $this->setLabels([
            'titlejap'      => 'Japán cím',
            'titleeng'      => 'Angol cím',
            'type'          => 'Típus',
            'status'        => 'Állapot',
            'firstepdate'   => 'Premier dátuma',
            'lastepdate'    => 'Utolsó epizód dátuma',
            'startdate'     => 'Kezdés',
            'finishdate'    => 'Befejezve',
            'episodenum'    => 'Epizódok száma',
            'episodelength' => 'Epizódok hossza',
            'description'   => 'Leírás'
        ]);

        $variables = get_object_vars($this);
        foreach ($variables as $key => $value){
            if(substr($key, 0, 1) != '_') $this->add($this->$key);
        }
    }
}