<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 09. 20.
 * Time: 7:25
 */
class Functions
{
    public static function getTimeFormat($time){
        $actTime = new DateTime();
        $actTime->setTimezone(new DateTimeZone("Europe/Budapest"));

        $first  = new DateTime($time);
        $second = new DateTime($actTime->format('Y-m-d h:i:s'));

        $diff = $first->diff( $second );

        $time = new stdClass();
        $time->second = (int)$diff->format( '%S' );
        $time->minute = (int)$diff->format( '%I' );
        $time->hour = (int)$diff->format( '%H' );
        $time->day = (int)$diff->format( '%d' );
        $time->month = (int)$diff->format( '%m' );
        $time->year = (int)$diff->format( '%Y' );

        if($time->year > 0) $elapsed = $time->year." éve";
        elseif ($time->month > 0) $elapsed = $time->month." hónapja";
        elseif ($time->day > 0) $elapsed = $time->day." napja";
        elseif ($time->hour > 0) $elapsed = $time->hour." órája";
        elseif ($time->minute > 0) $elapsed = $time->minute." perce";
        elseif ($time->second > 0) $elapsed = $time->second." másodperce";

        return $elapsed;
    }

    public static function minuteToHourMinute($minutes){
        $hour = intdiv($minutes, 60);
        $minute = $minutes % 60;

        return ($minute <= 0) ? $hour." óra" : $hour." óra, ".$minute." perc";
    }

    public static function getUrlFormat($string){
        $space = "-";

        $accents = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
        $characters = array(' ', '_', '.', ':', ';', '/', '[', ']', '!', '?', '(', ')');

        $string = strtr($string, $accents);
        $string = str_replace($characters, $space, $string);
        $string = str_replace('----', $space, $string);
        $string = str_replace('---', $space, $string);
        $string = str_replace('--', $space, $string);
        $string = trim(strtolower($string), $space);

        return $string;
    }

    public static function getIdFormat($string){
        return md5($string."animeworldByGorack");
    }

    public static function setNull($var){
        return ($var) ? $var : NULL;
    }

    public static function checkHasFiles($files){
        if(empty($files[0]->getTempName())) return false;
            else return true;
    }
}