<?php

/**
 * Created by PhpStorm.
 * User: Gorack
 * Date: 2017. 09. 11.
 * Time: 7:52
 */

use Phalcon\Forms\Element\Submit;

use Phalcon\Validation\Validator\Callback;

class LoginForm extends AuthForm
{
    public function initialize(){
        parent::initialize();

        $send = new Submit(
            'login',
            [
                'class' => 'btn btn-primary',
                'value' => 'Belépek'
            ]
        );

        $this->username->addValidator(
            new Callback(
                [
                    'message' => 'A megadott felhasználónévvel nem létezik fiók!',
                    'callback' => function($data){
                        if(!Users::findFirst('username ="'.$data['username'].'"')) return false;

                        return true;
                    }
                ]
            )
        );

        $this->add($this->username);
        $this->add($this->password);
        $this->add($send);
    }
}