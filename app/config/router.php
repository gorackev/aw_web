<?php

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group as RouterGroup;
use Phalcon\Mvc\Dispatcher;

$notFound = function ($uri){
    $path = BASE_PATH."/app/controllers/";
    $uri = explode('/', $uri);
    $controllerName = ucfirst($uri[1])."Controller";
    $fullPath = $path.$controllerName.".php";

    if(!is_file($fullPath)){
        $dispatcher = new Dispatcher();
        $dispatcher->forward([
            'controller' => 'news',
            'action' => 'index'
        ]);
    } else return true;
};

$router = new Router(false);

$router->setDefaults([
    'controller' => 'news',
    'action' => 'index'
]);
$router->add('/:controller', [
    'controller' => 1
])->beforeMatch(
    function($uri) use($notFound){
        if($notFound($uri)) return true;
    }
);
$router->add('/:controller/:action/:params', [
    'controller' => 1,
    'action' => 2,
    'params' => 3
])->beforeMatch(
    function($uri) use($notFound){
        if($notFound($uri)) return true;
    }
);


$groups['news'] = new RouterGroup([
    'controller' => 'news'
]);
$groups['news']->setPrefix('/news');
$groups['news']->add('/{url}', [
    'action' => 'show'
]);
$groups['news']->add('/category/{category}', [
    'action' => 'category'
]);

$groups['projects'] = new RouterGroup([
    'controller' => 'projects'
]);
$groups['projects']->setPrefix('/projects');
$groups['projects']->add('/{url}', [
    'action' => 'show'
]);
$groups['projects']->add('/{selectorType}/{selector}', [
    'action' => 'selector'
]);

$groups['admin'] = new RouterGroup([
    'controller' => 'admin'
]);
$groups['admin']->setPrefix('/admin');
$groups['admin']->add('/post/upload', [
    'action' => 'sendpost'
]);
$groups['admin']->add('/post/manage', [
    'action' => 'postmanage'
]);
$groups['admin']->add('/project/manage', [
    'action' => 'projectmanage'
]);
$groups['admin']->add('/project/upload', [
    'action' => 'sendproject'
]);
$groups['admin']->add('/post/edit/{id}', [
    'action' => 'editpost'
]);


foreach ($groups as $key => $value) $router->mount($groups[$key]);

$router->handle();