<?php

class Projects extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Primary
     * @Column(type="string", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $titlejap;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $titleeng;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $url;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $firstepdate;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $lastepdate;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $startdate;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $finishdate;

    /**
     *
     * @var integer
     * @Column(type="integer", length=3, nullable=true)
     */
    public $episodenum;

    /**
     *
     * @var integer
     * @Column(type="integer", length=3, nullable=true)
     */
    public $episodelength;

    /**
     *
     * @var string
     * @Column(type="string", length=32, nullable=false)
     */
    public $userid;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $description;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=false)
     */
    public $cover;

    public $duration;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("animeworld");
        $this->setSource("projects");
        $this->hasMany('id', 'ProjectGenre', 'projectId', ['alias' => 'ProjectGenre']);
        $this->hasMany('id', 'ProjectType', 'projectId', ['alias' => 'ProjectType']);
        $this->hasMany('id', 'ProjectStatus', 'projectId', ['alias' => 'ProjectStatus']);
        $this->belongsTo('userid', '\Users', 'id', ['alias' => 'Users']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'projects';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Projects[]|Projects|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Projects|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function beforeValidationOnCreate(){
        $this->url = Functions::getUrlFormat($this->titlejap);

        $this->lastepdate = Functions::setNull($this->lastepdate);
        $this->startdate = Functions::setNull($this->startdate);
        $this->finishdate = Functions::setNull($this->finishdate);

        $this->episodenum = (int)$this->episodenum;
        $this->episodelength = (int)$this->episodelength;
    }

    public function afterFetch(){
        $date = $this->firstepdate;
        $this->firstepdate = explode('-', $date);
        $this->firstepdate['full'] = $date;

        $none = "<i>nincs megadva</i>";
        if(!$this->lastepdate) $this->lastepdate = $none;
        if(!$this->startdate) $this->startdate = $none;
        if(!$this->finishdate) $this->finishdate = $none;
        if(!$this->description) $this->description = $none;

        $this->duration = (int)$this->episodenum*(int)$this->episodelength;

        if($this->duration > 60) $this->duration = Functions::minuteToHourMinute($this->duration);

    }

    public function addMessage($text, $field, $type){
        $this->appendMessage(new Message($text, $field, $type));
    }

}
