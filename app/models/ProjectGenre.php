<?php

class ProjectGenre extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Column(type="string", length=32, nullable=false)
     */
    public $projectId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $genreId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("animeworld");
        $this->setSource("project_genre");
        $this->belongsTo('projectId', '\Projects', 'id', ['alias' => 'Projects']);
        $this->belongsTo('genreId', '\ProjectGenres', 'id', ['alias' => 'ProjectGenres']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'project_genre';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProjectGenre[]|ProjectGenre|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProjectGenre|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
