<div class="box">
    <h5 class="text-muted"><i class="fa fa-user-plus fa-fw"></i>Regisztráció<hr></h5>
    {{form('/registration', 'enctype': 'multipart/form-data')}}
    {{form.renderDecorated('avatar')}}
    {{form.renderDecorated('username')}}
    <div class="row">
        <div class="col-12 col-md-6">{{form.renderDecorated('email')}}</div>
        <div class="col-12 col-md-6">{{form.renderDecorated('emailConfirm')}}</div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">{{form.renderDecorated('password')}}</div>
        <div class="col-12 col-md-6">{{form.renderDecorated('passwordConfirm')}}</div>
    </div>
    {{form.renderDecorated('birthdate')}}
    {{form.render('register')}}
    {{endform()}}
</div>