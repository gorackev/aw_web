<div class="postmanage">
    {{form('/admin/postmanage')}}
    <div class="controlpanel">
        <h4 class="text-muted">Kontrol panel</h4>
        <hr>
        <button type="submit" name="public" class="btn btn-primary"><i class="fa fa-share-alt fa-fw"></i>Publikálás</button>
        <button type="submit" name="delete" class="btn btn-danger"><i class="fa fa-close fa-fw"></i>Törlés</button>
    </div>

    <div class="postlist">
        <h4 class="text-muted">Várólista <hr></h4>
        {% for post in newposts %}
        <div class="post">
            <div class="col-2">
                {{check_field('check[]', 'value': post.id, 'id': post.id, 'style': 'display: none')}}
                <label class="checkbox" for="{{post.id}}"></label>
            </div>
            <div class="col-4"><p>{{post.title}}</p></div>
            <div class="col-3"><p>{{post.date.original}}</p></div>
            <div class="col-3">
                <a class="btn btn-warning" href="">Szerkesztés</a>
            </div>
        </div>
        {% endfor %}
    </div>

    <div class="postlist">
        <h4 class="text-muted">Bejegyzések <hr></h4>
        {% for post in posts %}
        <div class="post">
            <div class="col-2">
                {{check_field('check[]', 'value': post.id, 'id': post.id, 'style': 'display: none')}}
                <label class="checkbox" for="{{post.id}}"></label>
            </div>
            <div class="col-4"><p>{{post.title}}</p></div>
            <div class="col-3"><p>{{post.date.original}}</p></div>
            <div class="col-3">
                <a class="btn btn-warning" href="/admin/post/edit/{{post.id}}"><i class="fa fa-edit fa-fw"></i>Szerkesztés</a>
            </div>
        </div>
        {% endfor %}
    </div>
    {{endform()}}
</div>