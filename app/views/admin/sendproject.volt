<div class="box">
    <h4 class="text-muted"><i class="fa fa-plus fa-fw"></i>Új projekt felvétele <hr></h4>
    {{form('/admin/sendproject', 'enctype': 'multipart/form-data')}}
    <div class="form-group">
        <label for="cover">Borító</label>
        {{form.render('cover')}}
        <small id="coverHelp" class="form-text text-danger">{{messages.cover}}</small>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">{{form.renderDecorated('titlejap')}}</div>
        <div class="col-12 col-md-6">{{form.renderDecorated('titleeng')}}</div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">{{form.renderDecorated('type')}}</div>
        <div class="col-12 col-md-6">{{form.renderDecorated('status')}}</div>
    </div>
    <div class="form-check form-check-inline">
        <p class="m-0 mb-2"><b>Műfajok kiválasztása</b></p>
        {% for genre in form.genres %}
        <input class="animeGenre" type="checkbox" name="genres[]" id="{{genre.id}}" value="{{genre.id}}">
        <label class="form-check-label" for="{{genre.id}}">{{genre.name}}</label>
        {% endfor %}
        <hr>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">{{form.renderDecorated('firstepdate')}}</div>
        <div class="col-12 col-md-6">{{form.renderDecorated('lastepdate')}}</div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">{{form.renderDecorated('startdate')}}</div>
        <div class="col-12 col-md-6">{{form.renderDecorated('finishdate')}}</div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">{{form.renderDecorated('episodenum')}}</div>
        <div class="col-12 col-md-6">{{form.renderDecorated('episodelength')}}</div>
    </div>
    {{form.renderDecorated('description')}}
    {{form.render('send')}}
    {{endform()}}
</div>