<aside>
    {% if projectFilter %}
    {{ partial('layouts/aside/projectFilter') }}
    {% endif %}

    {% if latestNewsCheck %}
    {{ partial('layouts/aside/latestNews') }}
    {% endif %}
</aside>