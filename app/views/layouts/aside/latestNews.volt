<section class="latestNews">
    <h6 class="text-muted">LEGÚJABB BEJEGYZÉSEK</h6>
    <hr>
    {% for post in latestNews %}
    <p>
        <a href="/news/{{post.url}}">
            <span class="text-left" title="{{post.date.original}}"><i class="fa fa-clock-o fa-fw"></i>{{post.date.elapsed}}</span>
            <span>{{post.title}}</span>
        </a>
    </p>
    {% endfor %}
</section>