<div class="row">
    {% if message %}
    <h6 class="text-warning text-dark bg-white p-3 m-auto">{{message}}</h6>
    {% endif %}

    {% if !message %}
    {% for project in projects %}
    <div class="col-12 col-sm-4 col-md-3 col-lg-4 col-xl-3">
        <div class="project">
            <img class="img-thumbnail" src="{{project.cover}}" width="100%">
            <a class="datas" href="/projects/{{project.url}}">
                <p class="title">{{project.titlejap}}<br>({{project.firstepdate[0]}})</p>
                {% for type in project.projectType %}
                <p class="type"><i class="fa fa-television fa-fw"></i>{{type.projectTypes.name}}</p>
                {% endfor %}
                <div class="genres">
                    {% for genres in project.projectGenre %}
                    <span class="genre">{{genres.projectGenres.name}}</span>
                    {% endfor %}
                </div>
            </a>
        </div>
    </div>
    {% endfor %}
    {% endif %}
</div>