<div class="box fullProject">
    <h5 class="titles"><i class="fa fa-tags fa-fw"></i><span><?= $project->titlejap ?></span> | <span><?= $project->titleeng ?></span><hr></h5>
    <div class="row">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
            <img class="img-thumbnail" src="<?= $project->cover ?>" width="100%">
        </div>
        <div class="col-12 col-sm-6 col-md-8 col-lg-9">
            <?php foreach ($project->projectStatus as $status) { ?>
            <p><i class="fa fa-flag-o fa-fw"></i><b>Állapot:</b> <a href="/projects/status/<?= $status->projectStates->url ?>"><?= $status->projectStates->name ?></a> <hr></p>
            <?php } ?>
            <p class="dates">
                <i class="fa fa-calendar-o fa-fw"></i>
                <span><b>Premier:</b> <?= $project->firstepdate['full'] ?></span>
                <span><b>Utolsó epizód:</b> <?= $project->lastepdate ?></span>
                <span><b>Elkezdtük:</b> <?= $project->startdate ?></span>
                <span><b>Befejeztük:</b> <?= $project->finishdate ?></span>
            <hr>
            </p>
            <p><i class="fa fa-clock-o fa-fw"></i><b>Hossz:</b> <?= $project->episodenum ?> x <?= $project->episodelength ?> perc (<?= $project->duration ?>) <hr></p>
            <?php foreach ($project->projectType as $type) { ?>
            <p><i class="fa fa-tag fa-fw"></i><b>Típus:</b> <a href="/projects/types/<?= $type->projectTypes->url ?>"><?= $type->projectTypes->name ?></a> <hr></p>
            <?php } ?>
            <div class="genres">
                <p><i class="fa fa-renren fa-fw"></i><b>Műfajok:</b></p>
                <?php foreach ($project->projectGenre as $genres) { ?>
                <a href="/projects/genres/<?= $genres->projectGenres->url ?>" class="bg-info text-light p-1"><?= $genres->projectGenres->name ?></a>
                <?php } ?>
                <hr>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p><i class="fa fa-info-circle fa-fw"></i><b>Leírás:</b></p>
            <p><?= $project->description ?> <hr></p>
        </div>
    </div>
</div>