<?= $this->partial('layouts/navbar') ?>
<div class="container">
    <div class="registration">
        <?= $this->tag->form(['/signin/registration']) ?>

        <div class="form-group">
            <label for="username">Felhasználónév</label>
            <?= $form->render('username') ?>
            <small id="usernameHelp" class="form-text text-danger"><?= $messages->username ?></small>
        </div>
        <div class="form-group">
            <label for="password">Jelszó</label>
            <?= $form->render('password') ?>
            <small id="passwordHelp" class="form-text text-danger"><?= $messages->password ?></small>
        </div>
        <div class="form-group">
            <label for="passwordConfirm">Jelszó megerősítése</label>
            <?= $form->render('passwordConfirm') ?>
            <small id="passwordConfirmHelp" class="form-text text-danger"><?= $messages->passwordConfirm ?></small>
        </div>
        <div class="form-group">
            <label for="email">E-mail cím</label>
            <?= $form->render('email') ?>
            <small id="emaileHelp" class="form-text text-danger"><?= $messages->email ?></small>
        </div>
        <div class="form-group">
            <label for="emailConfirm">E-mail cím megerősítése</label>
            <?= $form->render('emailConfirm') ?>
            <small id="emailConfirmHelp" class="form-text text-danger"><?= $messages->emailConfirm ?></small>
        </div>
        <div class="form-group">
            <label for="birthDate">Születési dátum</label>
            <?= $form->render('birthdate') ?>
            <small id="birthdateHelp" class="form-text text-danger"><?= $messages->birthdate ?></small>
        </div>
        <?= $form->render('register') ?>

        <?= $this->tag->endform() ?>
    </div>
</div>