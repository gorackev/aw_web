<section class="projectFiltration">
    <h5 class="text-muted"><i class="fa fa-filter fa-fw"></i>Szűrés <hr></h5>
    <?= $this->tag->form(['/projects']) ?>
    <div class="form-group genres">
        <?php foreach ($form->genres as $genre) { ?>
        <input type="checkbox" id="<?= $genre->id ?>" class="animeGenre" name="genres[]" <?= $genre->checked ?> value="<?= $genre->id ?>">
        <label for="<?= $genre->id ?>"><?= $genre->name ?></label>
        <?php } ?>
        <hr>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->renderDecorated('type') ?>
        </div>
        <div class="col-6">
            <?= $form->renderDecorated('status') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->renderDecorated('startyear') ?>
        </div>
        <div class="col-6">
            <?= $form->renderDecorated('endyear') ?>
        </div>
    </div>
    <?= $form->render('send') ?>
    <?= $this->tag->endform() ?>
</section>