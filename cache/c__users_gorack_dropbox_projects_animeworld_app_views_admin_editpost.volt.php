<div class="box">
    <h5 class="text muted">Bejegyzés szerkesztése<hr></h5>
    <?= $this->tag->form(['enctype' => 'multipart/form-data']) ?>
    <div class="row">
        <div class="col-12 col-sm-6">
            <img class="img-thumbnail" src="<?= $post->cover ?>" width="100%">
        </div>
        <div class="col-12 col-sm-6">
            <?= $form->renderDecorated('cover') ?>
        </div>
    </div>
    <?= $form->renderDecorated('title') ?>
    <?= $form->renderDecorated('precursory') ?>
    <?= $form->renderDecorated('content') ?>
    <?= $form->renderDecorated('categoryId') ?>
    <?= $form->render('send') ?>
    <?= $this->tag->endform() ?>
</div>