<nav class="navbar bg-dark">
    <div class="col-12 col-sm-6">
        <ul class="float-left">
            <li><a class="logo" href=""><i>A<span>nime</span>W<span>orld</span></i></a></li>
            <li><a href="/news">hírek</a></li>
            <li><a href="/projektek">projektek</a></li>
        </ul>
    </div>
    <div class="col-12 col-sm-6">
        <ul class="float-right">
            <li><a href="">keresés</a></li>
            <li><a href="/signin">belépés</a></li>
        </ul>
    </div>
</nav>