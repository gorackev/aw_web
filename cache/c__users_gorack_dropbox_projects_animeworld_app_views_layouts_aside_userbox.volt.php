<section>
    <h6 class="text-muted">PROFILOD</h6>
    <hr>
    <div class="row">
        <div class="col-5"><p>Felhasználónév:</p></div>
        <div class="col-7"><p><?= $user->username ?></p></div>
    </div>
    <div class="row">
        <div class="col-5"><p>E-mail cím:</p></div>
        <div class="col-7"><p><?= $user->email ?></p></div>
    </div>
    <div class="row">
        <div class="col-12">
            <a class="btn btn-dark" href="/logout">Kijelentkezés</a>
        </div>
    </div>
</section>