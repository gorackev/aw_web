<?php foreach ($news as $post) { ?>
<div class="post">
    <div class="d-flex flex-wrap">
        <div class="col-12 col-sm-4 cover" style="background-image: url(<?= $post->cover ?>)"></div>
        <div class="col-12 col-sm-7 content">
            <div class="header">
                <p><a href="/news/<?= $post->url ?>"><?= $post->title ?></a></p>
            </div>
            <div class="main">
                <p><?= $post->precursory ?></p>
            </div>
            <div class="footer">
                <span title="<?= $post->date->original ?>"><i class="fa fa-clock-o fa-fw"></i><?= $post->date->elapsed ?></span>
                <?php foreach ($post->postCategory as $category) { ?>
                <span class="bg-primary p-1"><a class="text-light" href="/news/category/<?= $category->postCategories->url ?>"><i class="fa fa-bookmark-o fa-fw"></i><?= $category->postCategories->name ?></a></span>
                <?php } ?>
            </div>
        </div>
        <div class="col-12 col-sm-1">
            <div class="downloads">
                <a href="" title="Felirat"><i class="fa fa-file-text-o fa-2x"></i></a>
                <a href="" title="Torrent"><i class="fa fa-file-o fa-2x"></i></a>
                <a href="" title="AW videó"><i class="fa fa-file-movie-o fa-2x"></i></a>
                <a href="" title="Online megtekintés"><i class="fa fa-film fa-2x"></i></a>
            </div>
        </div>
    </div>
</div>
<?php } ?>