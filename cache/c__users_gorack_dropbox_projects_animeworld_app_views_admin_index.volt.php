<div class="box">
    <h5 class="text-muted">Bejegyzések<hr></h5>
    <a class="btn btn-primary text-white" href="/admin/post/upload"><i class="fa fa-plus fa-fw"></i>Új bejegyzés</a>
    <a class="btn btn-dark text-white" href="/admin/post/manage"><i class="fa fa-edit fa-fw"></i>Bejegyzések kezelése</a>

    <h5 class="mt-4 text-muted">Projektek<hr></h5>
    <a class="btn btn-primary text-white" href="/admin/project/upload"><i class="fa fa-plus fa-fw"></i>Új projekt</a>
    <a class="btn btn-dark text-white" href="/admin/project/manage"><i class="fa fa-edit fa-fw"></i>Projektek kezelése</a>
    <a class="btn btn-primary text-white" href="/admin/addprojectgenre"><i class="fa fa-plus fa-fw"></i>Új műfaj felvétel</a>
</div>