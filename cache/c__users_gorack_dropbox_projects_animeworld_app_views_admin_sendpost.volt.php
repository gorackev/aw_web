<div class="sendPost">
    <h4 class="text-muted">Új hír beküldése</h4>
    <hr>
    <?= $this->tag->form(['enctype' => 'multipart/form-data']) ?>
    <div class="form-group">
        <label for="title"><i class="fa fa-tag fa-fw"></i>Cím</label>
        <?= $form->render('title') ?>
        <small id="titleHelp" class="form-text text-danger"><?= $messages->title ?></small>
    </div>
    <div class="form-group">
        <label for="cover"><i class="fa fa-picture-o fa-fw"></i>Borítókép</label>
        <?= $form->render('cover') ?>
        <small id="coverHelp" class="form-text text-danger"><?= $messages->cover ?></small>
    </div>
    <div class="form-group">
        <label for="precursory"><i class="fa fa-eye fa-fw"></i>Előzetes</label>
        <?= $form->render('precursory') ?>
        <small id="precursoryHelp" class="form-text text-danger"><?= $messages->precursory ?></small>
    </div>
    <div class="form-group">
        <label for="content"><i class="fa fa-file-text-o fa-fw"></i>Tartalom</label>
        <?= $form->render('content') ?>
        <small id="contentHelp" class="form-text text-danger"><?= $messages->content ?></small>
    </div>
    <div class="form-group">
        <label for="categoryId"><i class="fa fa-bookmark-o fa-fw"></i>Kategória</label>
        <?= $form->render('categoryId') ?>
        <small id="categoryIdHelp" class="form-text text-danger"><?= $messages->categoryId ?></small>
    </div>

    <hr>
    <?= $form->render('send') ?>
    <?= $this->tag->endform() ?>
</div>