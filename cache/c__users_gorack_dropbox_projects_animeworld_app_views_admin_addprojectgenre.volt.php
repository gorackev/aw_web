<div class="box">
    <h4 class="text-muted"><i class="fa fa-plus-circle fa-fw"></i>Új projekt műfaj hozzáadása <hr></h4>
    <?= $this->tag->form(['/admin/addprojectgenre']) ?>
    <?= $form->render('name') ?>
    <small id="nameHelp" class="form-text text-danger"><?= $messages->name ?></small>
    <hr>
    <?= $form->render('send') ?>
    <?= $this->tag->endform() ?>
</div>