<aside>
    <?php if ($projectFilter) { ?>
    <?= $this->partial('layouts/aside/projectFilter') ?>
    <?php } ?>

    <?php if ($latestNewsCheck) { ?>
    <?= $this->partial('layouts/aside/latestNews') ?>
    <?php } ?>
</aside>