<div class="loginWrapper">
    <div class="login">
        <?= $this->tag->form(['/signin/login']) ?>

        <div class="form-group">
            <label for="username">Felhasználónév</label>
            <?= $form->render('username') ?>
            <small id="usernameHelp" class="form-text text-danger"><?= $messages->username ?></small>
        </div>
        <div class="form-group">
            <label for="password">Jelszó</label>
            <?= $form->render('password') ?>
            <small id="passwordHelp" class="form-text text-danger"><?= $messages->password ?></small>
        </div>
        <div class="form-group">
            <?= $this->tag->checkField(['remember']) ?>
            <label for="remember">Emlékezz rám</label>
        </div>
        <?= $form->render('login') ?>

        <?= $this->tag->endform() ?>
    </div>
</div>