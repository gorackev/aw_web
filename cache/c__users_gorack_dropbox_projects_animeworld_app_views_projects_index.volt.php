<div class="row">
    <?php if ($message) { ?>
    <h6 class="text-warning text-dark bg-white p-3 m-auto"><?= $message ?></h6>
    <?php } ?>

    <?php if (!$message) { ?>
    <?php foreach ($projects as $project) { ?>
    <div class="col-12 col-sm-4 col-md-3 col-lg-4 col-xl-3">
        <div class="project">
            <img class="img-thumbnail" src="<?= $project->cover ?>" width="100%">
            <a class="datas" href="/projects/<?= $project->url ?>">
                <p class="title"><?= $project->titlejap ?><br>(<?= $project->firstepdate[0] ?>)</p>
                <?php foreach ($project->projectType as $type) { ?>
                <p class="type"><i class="fa fa-television fa-fw"></i><?= $type->projectTypes->name ?></p>
                <?php } ?>
                <div class="genres">
                    <?php foreach ($project->projectGenre as $genres) { ?>
                    <span class="genre"><?= $genres->projectGenres->name ?></span>
                    <?php } ?>
                </div>
            </a>
        </div>
    </div>
    <?php } ?>
    <?php } ?>
</div>