<div class="fullPost">
    <h4><?= $post->title ?><hr></h4>
    <div class="row">
        <div class="col-12 col-md-4">
            <img class="img-thumbnail" src="<?= $post->cover ?>" width="100%" alt="">
        </div>
        <div class="col-12 col-md-8">
            <p>
                <?= $post->precursory ?>
            </p>
            <p>
                <span><i class="fa fa-clock-o fa-fw"></i><?= $post->date->elapsed ?></span>
                <?php foreach ($post->postCategory as $category) { ?>
                <span class="bg-primary p-1"><a class="text-light" href="/news/category/<?= $category->postCategories->url ?>"><i class="fa fa-bookmark-o fa-fw"></i><?= $category->postCategories->name ?></a></span>
                <?php } ?>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p><hr><?= $post->content ?></p>
        </div>
    </div>
</div>