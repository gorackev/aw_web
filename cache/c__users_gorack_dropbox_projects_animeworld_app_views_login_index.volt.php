<div class="signIn">
    <div class="row">
        <div class="col-7">
            <h6 class="text-muted">Belépés AW fiókkal<hr></h6>
            <?= $this->tag->form(['/login']) ?>
            <?= $form->renderDecorated('username') ?>
            <?= $form->renderDecorated('password') ?>
            <div class="form-check">
                <label class="form-check-label">
                    <?= $this->tag->checkField(['remember', 'class' => 'form-check-input']) ?>
                    Emlékezz rám
                </label>
            </div>
            <hr>
            <?= $form->render('login') ?>
            <?= $this->tag->endform() ?>
        </div>
        <div class="col-5">
            <h6 class="text-muted">Belépés más fiókkal<hr></h6>
            <p><i>Hamarosan...</i></p>
        </div>
    </div>
</div>