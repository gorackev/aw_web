<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="mainMenu">
            <ul class="navbar-nav mr-auto">
                <?php if ($isAdmin) { ?>
                <li class="nav-item"><a href="/admin"><i class="fa fa-user-secret fa-fw"></i>adminisztráció</a></li>
                <?php } ?>
                <li class="nav-item"><a href="/news"><i class="fa fa-home fa-fw"></i>hírek</a></li>
                <li class="nav-item"><a href="/projects"><i class="fa fa-globe fa-fw"></i>projektek</a></li>
                <li class="nav-item"><a href="/sentinel"><i class="fa fa-eye fa-fw"></i>követő</a></li>
                <li class="nav-item"><a href="/admission"><i class="fa fa-user-plus fa-fw"></i>tagfelvétel</a></li>
                <li class="nav-item"><a href="/about"><i class="fa fa-info-circle fa-fw"></i>rólunk</a></li>
            </ul>
        </div>
    </div>
</nav>