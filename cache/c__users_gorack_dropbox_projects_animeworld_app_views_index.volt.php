<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/img/aw-logo-2.png">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?= $title ?></title>
        <?= $this->assets->outputCss() ?>
    </head>
    <body>
        <?= $this->flashSession->output() ?>
        <header class="container-fluid bg-dark">
            <div class="userbox">
                <?php if (!$user) { ?>
                <div class="sign">
                    <a href="/login" title="Bejelentkezés"><i class="fa fa-sign-in fa-fw"></i></a>
                    <a href="/registration" title="Regisztráció"><i class="fa fa-user-plus fa-fw"></i></a>
                </div>
                <?php } ?>
                <?php if ($user) { ?>
                <div class="user">
                    <div class="avatar"><img src="<?= $user->cover ?>" title="<?= $user->username ?>" alt="<?= $user->username ?>"></div>
                    <div class="datas">
                        <a class="btn btn-info" href="/profile"><i class="fa fa-user-md fa-fw"></i>Profil</a>
                        <a class="btn btn-info" href="/userconfig"><i class="fa fa-cog fa-fw"></i>Beállítások</a>
                        <a class="btn btn-info" href="/logout"><i class="fa fa-sign-out fa-fw"></i>Kijelentkezés</a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </header>
        <?= $this->partial('layouts/mainMenu') ?>
       <div class="container-fluid">
           <div class="row">
               <?php if ($sidebar) { ?>
               <div class="col-12 col-lg-4">
                   <?= $this->partial('layouts/aside/index') ?>
               </div>
               <div class="col-12 col-lg-8">
                   <content><?= $this->getContent() ?></content>
               </div>
               <?php } ?>
               <?php if (!$sidebar) { ?>
               <div class="container"><?= $this->getContent() ?></div>
               <?php } ?>
           </div>
       </div>

        <footer class="container-fluid">
            <p class="text-center text-light"><i class="fa fa-copyright fa-fw"></i>2017 AnimeWorld</p>
        </footer>

        <?= $this->assets->outputJs() ?>
        <script type="text/javascript">
            CKEDITOR.replace( 'editor', {
                language: 'hu',
                uiColor: ''
            });
        </script>
    </body>
</html>
